/**
 * @type {String}
 * @properties={typeid:35,uuid:"E403EA4B-5481-4B2A-914C-BD8EAD703E97"}
*/
var VERSION = '1.1.1';


/**
 * Flip to true for debugging mode. In debugging mode all exceptions are processed synchronously, so full stack traces are preserved
 * @private 
 *
 * @properties={typeid:35,uuid:"07F678D2-8480-4F37-9A8A-BD6D134BBA7B",variableType:-4}
 */
var DEBUG = false;

/**
 * Local uid tracker
 * @type {Number}
 *
 * @private 
 *
 * @properties={typeid:35,uuid:"8689F600-1127-4F01-8EC6-2E8775922CBF",variableType:4}
 */
var uid = 0;

/**
 * @type {{String:Array<{token:Number, callback:function, connection:function}>}}
 * 
 * @private 
 *
 * @properties={typeid:35,uuid:"7BDBEF4B-4815-444C-A00A-C370DEEB306F",variableType:-4}
 */
var subscribers = {};

/**
 * @type {{String:Array<{token:Number, callback:function, connection:function}>}}
 * 
 * @private 
 * @properties={typeid:35,uuid:"F09C900F-6CE4-4F97-AEBF-270C596821EB",variableType:-4}
 */
var regulators = {};

/**
 * Add a subscriber to a channel
 * 
 * @param {String} sChannel
 * @param {function(String, ...*)} fSubscriber
 * @param {function(*)} [fOnError] 
 * 
 * @return {String} Subscription Token
 *
 * @properties={typeid:24,uuid:"65EA6F68-5D3B-4CE2-A59B-9A01C129511B"}
 */
function subscribe(sChannel, fSubscriber, fOnError) {
	//Allow for empty string channels
	if (sChannel == undefined  || !fSubscriber) {
		return null;
	}
	
	if ( ! subscribers.hasOwnProperty(sChannel) ) {
		subscribers[sChannel] = [];
	}
	
	var sToken = (++uid).toString(36);
	subscribers[sChannel].push({
		token : sToken,
		callback : fSubscriber,
		connection : createConnection(fSubscriber, fOnError, true)
	});
	
	return sToken;
}

/**
 * Broadcast any number of arguments to subscribers on a given channel
 * @param {String} sChannel
 * @param {...*} [arg]
 *
 * @properties={typeid:24,uuid:"46474FD1-32C0-41C6-B09A-0DB1C0F9657C"}
 */
function broadcast(sChannel) {
	//Allow for empty string channels
	if (sChannel == undefined) {
		return;
	}
	
	//Send message to increasingly broad channels
	for (var c = sChannel; c != null; c = tune(c)) {
		broadcastMessage(c, arguments);
	}
}

/**
 * Unsubscribe to a specific subscription token, or to any subscriptions matching a given subscriber function
 * @param {String|Function} oTokenOrReceiver
 *
 * @return {Boolean}
 * 
 * @properties={typeid:24,uuid:"BD071505-761B-4E58-89EF-58FAF486D649"}
 */
function unsubscribe(oTokenOrReceiver) {
	if (oTokenOrReceiver == undefined) {
		return false;
	}
	
	return removeTokenOrFunction(subscribers,oTokenOrReceiver);
}

/**
 * Submit a request to prohibitors on a given channel 
 * @param {String} sChannel
 * @param {...*} [arg]
 * 
 * @return {Boolean}
 * 
 * @throws {Error}
 *
 * @properties={typeid:24,uuid:"8B2AA53D-442F-45C1-BFF8-6127AE26C130"}
 */
function sanction(sChannel) {
	if (sChannel == undefined) {
		return false;
	}

	//Acknowledge responses on increasingly broad channels
	for (var c = sChannel; c != null; c = tune(c)) {
		if (!requestPermission(c, arguments)) {
			return false
		}
	}
	return true;
}


/**
 * Register a listener on a given channel to prohibit the action requested.
 * 
 * @param {String} sChannel
 * @param {function(String, ...*):Boolean} fRegulator
 * @return {String} Subscription Token
 
 *
 * @properties={typeid:24,uuid:"39BE8FE0-35E1-47C4-9600-0183CF60C91C"}
 */
function regulate(sChannel, fRegulator) {
	if (!sChannel && fRegulator) {
		return null;
	}
	
	if ( ! regulators.hasOwnProperty(sChannel) ) {
		regulators[sChannel] = [];
	}
	
	var sToken = (++uid).toString(36);
	regulators[sChannel].push({
		token : sToken,
		callback : fRegulator,
		connection : createConnection(fRegulator)
	});
	
	return sToken;
}

/**
 * Deregulate a specific subscription token, or to any subscriptions matching a given regulator function
 * @param {String|Function} oTokenOrRegulator
 *
 * @return {Boolean}
 * 
 *
 * @properties={typeid:24,uuid:"AD689F2C-0425-44B1-AF35-3B709027164C"}
 */
function deregulate(oTokenOrRegulator) {
	if (oTokenOrRegulator == undefined) {
		return false;
	}
	
	return removeTokenOrFunction(regulators, oTokenOrRegulator);
}

/**
 * Tune a channel to a broader range
 * 
 * @param {String} sChannel
 *
 * @return {String}
 * 
 * @example tune('a.b.c.d') == 'a.b.c'; 
 * @example tune('a') == ''
 * @example tune('') == null
 * 
 * @private 
 * 
 * @properties={typeid:24,uuid:"8BD58E9E-8142-4C38-ABD9-18F75D50CFFE"}
 */
function tune(sChannel) {
	if (sChannel == null || sChannel == "") {
		return null;
	}
	
	var nPos = sChannel.lastIndexOf('.');
	sChannel = sChannel.substr(0, nPos);
	
	return sChannel;
}

/**
 * Removes a token or function from a callback collection
 * @param {{String:Array}} oCollection
 * @param {String|Function} oTokenOrFunction
 *
 * @return {Boolean}
 * 
 * @private 
 * 
 * @properties={typeid:24,uuid:"E39F071C-7E67-4C6A-B20A-0416D05DAA9C"}
 */
function removeTokenOrFunction(oCollection, oTokenOrFunction) {
	
	var bIsToken = (typeof oTokenOrFunction != 'function');
	var sKey = bIsToken ? 'token' : 'callback';

	var bResult = false;
	
	for (var sChannel in subscribers) {
		if ( !oCollection.hasOwnProperty(sChannel) || oCollection[sChannel].length == 0 ) {
			continue;
		}
		
		//Loop backwards through subscribers to remove any that match the request 
		for (var i = oCollection[sChannel].length - 1 ; i >= 0; i--) {
			if (oCollection[sChannel][i][sKey] === oTokenOrFunction) {
				oCollection[sChannel].splice(i, 1);
				bResult = true;
				
				//Only one subscription per token, return immediately
				if (bIsToken) {
					return bResult;
				}
			}
		}
	}
	return bResult;	
}


/**
 * Emit a message array to subscribers on an individual channel
 * @param {String} sChannel
 * @param {Array} aMessage
 * 
 * @private 
 *
 * @properties={typeid:24,uuid:"9802BA87-1412-45DC-BB10-A11B4D7A0106"}
 */
function broadcastMessage(sChannel, aMessage) {
	if (! subscribers.hasOwnProperty(sChannel) || subscribers[sChannel].length == 0) {
		return;
	}
	
	//Extract the connections before delivery. Avoids looping issues if a callback unsubscribes synchronously.
	/** @type {Array<Function>} */
	var aConnection = subscribers[sChannel].map(getConnection);
	aConnection.forEach(deliverMessage(aMessage));
}

/**
 * Performs a request to all prohibitors, returning false as soon as one provides a deny message, or true if all provide no response
 * @param {String} sChannel
 * @param {Array} aMessage
 * 
 * @return {Boolean}
 * @private 
 *
 *
 * @properties={typeid:24,uuid:"1FBB0465-8D0C-4C33-82AC-8C0DFD6D8B7E"}
 */
function requestPermission(sChannel, aMessage) {
	if (! regulators.hasOwnProperty(sChannel) || regulators[sChannel].length == 0) {
		return true;
	}
	
	//Extract the connections before delivery. Avoids looping issues if a responder unregisters synchronously.
	/** @type {Array<Function>} */
	var aConnection = regulators[sChannel].map(getConnection);
	return aConnection.every(deliverMessage(aMessage));
}

/**
 * Retrieve the connection function from a connector.
 * @param {{connection:Function}} o
 * 
 * @return {Function}
 * 
 * @private 
 * @properties={typeid:24,uuid:"815B8BA3-DEB4-4F35-B2E9-3DD43CEB4C43"}
 */
function getConnection(o) {
	return o.connection;
}

/**
 * Provides a function that delivers a given message to supplied functions
 * @param {Array} aMessage
 * 
 * @return {function(function)}
 *
 * @private 
 *
 * @properties={typeid:24,uuid:"AE0DC76F-5822-41A8-8856-833FDA8440D5"}
 */
function deliverMessage(aMessage) {
	return function(f) {
		return f.apply(null, aMessage);
	}
}

/**
 * Create a connection function for a callback and error handler combination. 
 * A connection acts as a controlled execution environment that a broadcaster can 
 * safely pass messages thru without needing to worry about exceptions that may occur
 *  
 * @param {function(String, ...*)} fCallback
 * @param {function(*)} [fOnError] Defaults to throwing the exception
 * @param {Boolean} [bAsyncError]
 * @return {function(String, ...*)}
 * 
 * @private 
 * 
 * @properties={typeid:24,uuid:"41E597BF-4E0C-448A-9F6E-0557298EF46E"}
 */
function createConnection(fCallback, fOnError, bAsyncError) {
	fOnError = fOnError || throwException;
	//If in debug mode, throw exceptions synchronously to keep a better stack trace.
	var fExceptionHandler = (DEBUG || !bAsyncError) ? fOnError : async(fOnError);
	return function() {
		try {
			return fCallback.apply(null, arguments);
		}
		catch (e) {
			fExceptionHandler(e);
		}
	}
}

/**
 * Creates an asynchronous version of a given function
 * @param {function} f
 *
 * @return {function}
 * 
 * @private
 *
 * @properties={typeid:24,uuid:"A4D536C4-098B-488A-955B-24B8C773ED24"}
 */
function async(f) {
	return function () {
		setTimeout(f, 1, arguments);
	}
}

/**
 * Throw an exception
 * 
 * @param {*} e Exception object to throw
 * 
 * @throws {*}
 *
 * @private 
 *
 * @properties={typeid:24,uuid:"0BB462F9-8A99-4CD3-AEE2-FB2569AE7B31"}
 */
function throwException(e) {
	throw e;
}

/**
 * 
 * @param {Function} f
 * @param {Number} nMS
 * @param {Array} [aArg]
 *
 * @return {String}
 * 
 * @private 
 *
 * @properties={typeid:24,uuid:"5F9D1380-6DB5-4F12-9F4B-C3F7525AAA35"}
 */
function setTimeout(f, nMS, aArg) {
	var dStart = new Date();
	dStart.setMilliseconds(dStart.getMilliseconds() + nMS);
	var sID = application.getUUID().toString();
	plugins.scheduler.addJob(sID, dStart, timeoutHandler, 0, 0, dStart, [f, aArg]);
	return sID;
}

/**
 * Calling handler for setTimeout. This is needed because the scheduler plugin only accepts Servoy functions
 * 
 * @param {Function} fCallback 
 * @param {Array} [aArg]
 * 
 * @private 
 *
 * @properties={typeid:24,uuid:"9B8B134F-CED7-4BC8-8C76-4175E59F2029"}
 */
function timeoutHandler(fCallback, aArg){
	fCallback.apply(null, aArg);
}

/**
 * Callback method for data broadcast.
 *
 * @param {String} sDataSource table data source
 * @param {Number} nAction see SQL_ACTION_TYPES constants
 * @param {JSDataSet} oPKDataset affected primary keys
 * @param {Boolean} bCached data was cached
 * 
 * @deprecated 
 *
 * @properties={typeid:24,uuid:"5CFD049D-6F10-47A5-9BCF-7524CEC49F6D"}
 */
function rebroadcastDataBroadcast(sDataSource, nAction, oPKDataset, bCached) {
	var sChannel = makeDatabroadcastChannel(sDataSource,nAction);
	broadcast(sChannel, oPKDataset, bCached);
}

/**
 * Subscribe to databroadcast events
 * @param fReceiver
 * @param [sDataSource] If not provided, subscribes to *all* databroadcast events
 * @param [nAction] If provided, subscribes to a specific SQL_ACTION_TYPES action
 *
 * @deprecated 
 * @properties={typeid:24,uuid:"63183F89-5B3D-4F32-BCF6-F5A4EADF8D01"}
 */
function subscribeToDataBroadcast(fReceiver, sDataSource, nAction) {
	if (!fReceiver) {
		return null;
	}
	
	var sChannel = makeDatabroadcastChannel(sDataSource,nAction);
	return subscribe(sChannel,fReceiver);
}

/**
 * Create a databroadcast channel identifier
 * @param {String} [sDataSource]
 * @param {Number} [nAction]
 * 
 * @return {String}
 * 
 * @deprecated 
 * @properties={typeid:24,uuid:"3DAE590C-D790-4AB5-ACFF-D274506C5E52"}
 */
function makeDatabroadcastChannel(sDataSource, nAction) {
	if (sDataSource == undefined) {
		return 'databroadcast';
	}
	
	if (nAction == undefined) {
		return [
			'databroadcast',
			databaseManager.getDataSourceServerName(sDataSource),
			databaseManager.getDataSourceTableName(sDataSource)
		].join('.');
	}
	
	return [
		'databroadcast',
		databaseManager.getDataSourceServerName(sDataSource),
		databaseManager.getDataSourceTableName(sDataSource),
		nAction.toString(10)
	].join('.');
}

/**
 * Extract a datasource from a channeljs databroadcast channel.
 * @param {String} sChannel
 * 
 * @return {String}
 * 
 * @deprecated 
 * @properties={typeid:24,uuid:"D501110A-F010-4619-9309-C6F57EAECCBB"}
 */
function getDataSourceFromChannel(sChannel) {
	if (!sChannel) {
		return '';
	}
	
	/** @type {Array<String>} */
	var aFrequency = sChannel.split('.');
	
	if (aFrequency.length < 3) {
		return '';
	}
	
	var sDataSource = ['db:', aFrequency[1], aFrequency[2]].join('/');
	
	return sDataSource;
}