/**
 * @type {String}
 * @properties={typeid:35,uuid:"E48FC61B-A6EC-4545-87E6-7A14423E7A8B"}
*/
var VERSION = '1.2.0';


/**
 * @properties={typeid:35,uuid:"BC6EF166-DD73-4467-A504-CE5314535825",variableType:-4}
 */
var TABLE_EVENT = {
	INSERT : 'insert',
	UPDATE : 'update',
	DELETE : 'delete',
	CREATE : 'create',
	FIND: 'find',
	SEARCH: 'search'
}

/**
 * Record after-insert trigger.
 *
 * @param {JSRecord<db:/adsails/aaa_contact>} record record that is inserted
 *
 * @properties={typeid:24,uuid:"3288B738-47ED-4F28-80BD-07CF9C07860D"}
 */
function afterRecordInsert(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePostEventChannel(sDataSource, TABLE_EVENT.INSERT);
	scopes.channeljs.broadcast(sChannel, record);
	return true
}

/**
 * Record after-update trigger.
 *
 * @param {JSRecord<db:/adsails/aaa_contact>} record record that is updated
 *
 * @properties={typeid:24,uuid:"87B82EAC-9469-49AF-9909-E08412FB8D76"}
 */
function afterRecordUpdate(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePostEventChannel(sDataSource, TABLE_EVENT.UPDATE);
	scopes.channeljs.broadcast(sChannel, record);
	return true
}

/**
 * Record after-delete trigger.
 *
 * @param {JSRecord<db:/adsails/aaa_contact>} record record that is deleted
 *
 * @properties={typeid:24,uuid:"2CB8D890-AFBF-4BAB-9564-ED8DB88962F9"}
 */
function afterRecordDelete(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePostEventChannel(sDataSource, TABLE_EVENT.DELETE);
	scopes.channeljs.broadcast(sChannel, record);
	return true
}

/**
 * Create a databroadcast channel identifier
 * @param {String} [sDataSource]
 * @param {Number} [nAction] SQL_ACTION_TYPES
 * 
 * @return {String}
 *  
 * @properties={typeid:24,uuid:"5DACCFB2-385D-4CB4-957F-5D34CC2B6BA4"}
 */
function makeDatabroadcastChannel(sDataSource, nAction) {
	if (sDataSource == undefined) {
		return 'databroadcast';
	}
	
	if (nAction == undefined) {
		return [
			'databroadcast',
			databaseManager.getDataSourceServerName(sDataSource),
			databaseManager.getDataSourceTableName(sDataSource)
		].join('.');
	}
	
	return [
		'databroadcast',
		databaseManager.getDataSourceServerName(sDataSource),
		databaseManager.getDataSourceTableName(sDataSource),
		nAction.toString(10)
	].join('.');
}

/**
 * Make a pre-event channel
 * @param {String} sDataSource
 * @param {String} sAction ACTION
 *
 * @properties={typeid:24,uuid:"125708DB-9AAC-43CD-BAE1-4C0310AAF225"}
 */
function makePreEventChannel(sDataSource, sAction) {
	if (sAction) {
		return [sDataSource, 'on', sAction].join('.');
	}
	else {
		return [sDataSource, 'on'].join('.');
	}
}

/**
 * Make a post-event channel
 * @param {String} sDataSource
 * @param {String} [sAction] ACTION
 *
 * @properties={typeid:24,uuid:"76FBB849-5D8C-447D-BAF9-355A449438FD"}
 */
function makePostEventChannel(sDataSource, sAction) {
	if (sAction) {
		return [sDataSource, 'after', sAction].join('.');
	}
	else {
		return [sDataSource, 'after'].join('.');
	}
}

/**
 * Broadcasts an INSERT event before it happens
 * 
 * When false is returned the record will not be inserted in the database.
 * When an exception is thrown the record will also not be inserted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord} record record that will be inserted
 *
 * @returns {Boolean}
 * 
 * @history	04/08/2015	MSF #AS-3412 Made public, renamed to "before" from "on", redocumented
 *
 * @properties={typeid:24,uuid:"E35C2BCC-28A8-4907-96AB-58622CFC8D17"}
 */
function beforeRecordInsert(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePreEventChannel(sDataSource, TABLE_EVENT.INSERT);
	scopes.channeljs.broadcast(sChannel, record);
	return true
}

/**
 * Broadcasts an UPDATE event before it happens
 *  
 * When false is returned the record will not be updated in the database.
 * When an exception is thrown the record will also not be updated in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord} record record that will be updated
 *
 * @returns {Boolean}
 * 
 * @history	04/08/2015	MSF #AS-3412 Made public, renamed to "before" from "on", redocumented
 *
 * @properties={typeid:24,uuid:"EA1483F0-5739-4E6E-A339-A259B88A36A4"}
 */
function beforeRecordUpdate(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePreEventChannel(sDataSource, TABLE_EVENT.UPDATE);
	scopes.channeljs.broadcast(sChannel, record);
	return true
}

/**
 * Broadcasts a DELETE event before it happens
 * 
 * Must return true as When false is returned the record will not be deleted in the database.
 * When an exception is thrown the record will also not be deleted in the database but it will be added to databaseManager.getFailedRecords(),
 * the thrown exception can be retrieved via record.exception.getValue().
 *
 * @param {JSRecord} record The record to be deleted
 *
 * @returns {Boolean}
 * 
 * @history	04/08/2015	MSF #AS-3412 Made public, renamed to "before" from "on", redocumented
 *
 * @properties={typeid:24,uuid:"BE59582B-790F-4B29-9524-628F37BF85C7"}
 */
function beforeRecordDelete(record) {
	var sDataSource = record.getDataSource();
	var sChannel = makePreEventChannel(sDataSource, TABLE_EVENT.DELETE);
	scopes.channeljs.broadcast(sChannel, record);
	return true;
}

/**
 * Callback method for data broadcast.
 *
 * @param {String} sDataSource table data source
 * @param {Number} nAction see SQL_ACTION_TYPES constants
 * @param {JSDataSet} oPKDataset affected primary keys
 * @param {Boolean} bCached data was cached
 * 
 *
 * @properties={typeid:24,uuid:"E0964BE7-7E70-437D-9936-06B40E269F74"}
 */
function rebroadcastDataBroadcast(sDataSource, nAction, oPKDataset, bCached) {
	var sChannel = makeDatabroadcastChannel(sDataSource,nAction);
	scopes.channeljs.broadcast(sChannel, oPKDataset, bCached);
}

/**
 * Subscribe to databroadcast events
 * @param {function(String, JSDataSet, Boolean)} fReceiver
 * @param {String} [sDataSource] If not provided, subscribes to *all* databroadcast events
 * @param {Number} [nAction] If provided, subscribes to a specific SQL_ACTION_TYPES action
 *
 * @return {String} subscription token
 *
 * @properties={typeid:24,uuid:"718F51CD-661D-487E-8655-2311283608F2"}
 */
function subscribeToDataBroadcast(fReceiver, sDataSource, nAction) {
	if (!fReceiver) {
		return null;
	}
	
	var sChannel = makeDatabroadcastChannel(sDataSource,nAction);
	return scopes.channeljs.subscribe(sChannel,fReceiver);
}

/**
 * Register a record DELETE subscriber
 * 
 * @param {function(String, String, *)} fSubscriber
 * @param {String} sDataSource
 * @param {String} sAction ACTION
 * 
 * @return {String}
 * 
 * @deprecated Use subscribeToTablePreEvent. 
 *
 * @history	04/08/2015	MSF #AS-3412 Created Happy birthday Dad!
 *
 * @properties={typeid:24,uuid:"CA8E73B1-4B28-4A62-91CB-45E705A77FC8"}
 */
function registerPreEventSubscriber(fSubscriber, sDataSource, sAction) {
	if ( !(fSubscriber && sDataSource) ) {
		return null;
	}
	var sChannel = makePreEventChannel(sDataSource, sAction);
	
	return scopes.channeljs.subscribe(sChannel, fSubscriber);
}

/**
 * Subscribe to Table based "Pre-Action" events
 *  
 * @param {function(String, JSRecord)} fSubscriber
 * @param {String} [sDataSource] If not provided, subscribes to *all* pre-events, for all tables
 * @param {String} [sAction] TABLE_EVENTS If provided, subscribes to a specific TABLE_EVENT action, otherwise, subscription is for all events
 *
 * @return {String} subscription id
 *
 * @properties={typeid:24,uuid:"7642EF13-DE88-4B4F-86A7-BE44E9113F5A"}
 */
function subscribeToTablePreEvent(fSubscriber, sDataSource, sAction) {
	if (!fSubscriber) {
		return null;
	}
	
	var sChannel = makePreEventChannel(sDataSource, sAction)
	
	return scopes.channeljs.subscribe(sChannel, fSubscriber);
}

/**
 * Subscribe to Table based "Post-Action" events
 *  
 * @param {function(String, JSRecord)} fSubscriber
 * @param {String} [sDataSource] If not provided, subscribes to *all* post-events for all tables
 * @param {String} [sAction] TABLE_EVENTS If provided, subscribes to a specific TABLE_EVENT action, otherwise, subscription is for all events
 *
 * @return {String} subscription id
 *
 * @properties={typeid:24,uuid:"4D18DFAF-2E36-447B-A3FA-108192D8369E"}
 */
function subscribeToTablePostEvent(fSubscriber, sDataSource, sAction) {
	if (!fSubscriber) {
		return null;
	}
	
	var sChannel = makePostEventChannel(sDataSource, sAction)
	
	return scopes.channeljs.subscribe(sChannel, fSubscriber);
}