/**
 * @properties={typeid:24,uuid:"00BF5326-69B5-4880-B382-480A8B18FE40"}
 */
function test_subscribe() {
	
	var sToken = scopes.channeljs.subscribe();
	jsunit.assertNull("Should return null when no channel is supplied", sToken);
	
	sToken = scopes.channeljs.subscribe('a.b.c.d');
	jsunit.assertNull("Should return null when no subscriber is supplied", sToken);
	
	sToken = scopes.channeljs.subscribe('a.b.c.d', servoySubscriber);
	jsunit.assertNotNull("Should allow Servoy Function subscribers", sToken);
	jsunit.assertEquals("Should return a string subscription token", 'string', typeof sToken);
	scopes.channeljs.unsubscribe(sToken);
	
	var nCounter = 0;
	sToken = scopes.channeljs.subscribe('a.b.c.d',function (sChannel) { 
		application.output(sChannel);
		nCounter++;
	});
	jsunit.assertNotNull("Should allow inline and closure function subscribers", sToken);
	jsunit.assertEquals("Should return a string subscription token", 'string', typeof sToken);
	
	
	scopes.channeljs.broadcast('a.b.c.d');
	jsunit.assertEquals("Subscriber should recieve messages", 1, nCounter);
	scopes.channeljs.unsubscribe(sToken);
	
	
	sToken = scopes.channeljs.subscribe("", function (sChannel) { 
		application.output(sChannel);
		nCounter++ 
	});
	jsunit.assertEquals("Should allow subscription to 'all channels'", 'string', typeof sToken);
	
	nCounter = 0;
	scopes.channeljs.broadcast('a.b.c.d');
	scopes.channeljs.broadcast('simple');
	scopes.channeljs.broadcast('');
	jsunit.assertEquals("Subscriber should recieve messages on all channels", 3, nCounter);
	scopes.channeljs.unsubscribe(sToken);	
}

/**
 * Empty channel subscriber for tests
 * @param {String} sChannel
 *
 * @properties={typeid:24,uuid:"0CAC9C0C-227D-4620-8161-D6844BC3A2E5"}
 */
function servoySubscriber(sChannel) {
	application.output('ServoySubScriber: ' + sChannel);
}


/**
 * @properties={typeid:24,uuid:"7BC7F293-2324-4282-845F-FC8C86A25304"}
 */
function test_broadcast() {
	//Turn debug mode on to deal with exceptions synchronously for testing.
	scopes.channeljs.DEBUG = true;
	
	var bResult = scopes.channeljs.broadcast();
	jsunit.assertFalse("Should return false when no channel is provided", bResult);
	
	bResult = scopes.channeljs.broadcast('myChannel');
	jsunit.assertTrue("Should return true when any channel is provided", bResult);
	
	
	//Add an all channels subscriber to test over time.
	var nAllChannelsCounter = 0;
	var nAllChannelsExpected = 0;
	var sAllChan = scopes.channeljs.subscribe('', function (sChannel) { 
		application.output('AllChannels: [' + sChannel + '], Args: ' + arguments.length);
		nAllChannelsCounter++; 
	});
	
	//Add a subscriber on a simple channel
	var nSimpleCounter = 0;
	var sSimpleChan = scopes.channeljs.subscribe('simple', function (sChannel) { 
		application.output('Simple: [' + sChannel + ']');
		nSimpleCounter++; 
	});
	
	//Test current channels
	scopes.channeljs.broadcast('simple');
	jsunit.assertEquals("Simple channel subscriber should be called once", 1, nSimpleCounter);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	//Remove simple channel subscription
	scopes.channeljs.unsubscribe(sSimpleChan);
	
	
	/**
	 * Test that calling channels get passed to subscribers listening on super-channels
	 */
	var sExpectedChannel;
	var sSuperChan = scopes.channeljs.subscribe('a.b', function (sChannel) {
		jsunit.assertEquals("Calling channel should get passed to subscribers on super-channels", sExpectedChannel, sChannel);
	});
	
	sExpectedChannel = 'a.b.c';
	
	scopes.channeljs.broadcast(sExpectedChannel);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	sExpectedChannel = 'a.b.c.d';
	scopes.channeljs.broadcast(sExpectedChannel);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	scopes.channeljs.unsubscribe(sSuperChan);
	
	
	var nMultiCounter = 0;
	var fMulti = function (sChannel) {
		application.output('Multi: [' + sChannel + ']');
		nMultiCounter++;
	}
	
	//Subscribe to a complex channel, in multiple ranges, with the same function
	scopes.channeljs.subscribe('a.b.c.d', fMulti);
	scopes.channeljs.subscribe('a.b.c', fMulti);
	scopes.channeljs.subscribe('a.b', fMulti);
	scopes.channeljs.subscribe('a', fMulti);
	
	scopes.channeljs.broadcast('a.b.c.d');
	jsunit.assertEquals("Subscriber should once on each channel part.", 4, nMultiCounter);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	nMultiCounter = 0;
	scopes.channeljs.broadcast('a.b.c');
	jsunit.assertEquals("Subscriber should once on each channel part.", 3, nMultiCounter);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	//Unsubscribe from complex channel using the subscriber function
	scopes.channeljs.unsubscribe(fMulti);
	
	nMultiCounter = 0;
	scopes.channeljs.broadcast('a.b.c.d');
	jsunit.assertEquals("Complex channel subscribers should no longer be called", 0, nMultiCounter);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	var nSum = 0;
	var sArgChan = scopes.channeljs.subscribe('argTest', function (sChannel, nVal) {
		application.output('Single Arg: ' + sChannel);
		nSum += nVal;
	});
	
	scopes.channeljs.broadcast('argTest', 4);
	jsunit.assertEquals("Arguments should be passed to subscribers", 4, nSum);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	var nComplexSum = 0;
	var sMoreArgChan = scopes.channeljs.subscribe('argTest', function (sChannel) {
		application.output('Splat: [' + sChannel + ']');
		for (var i = 1; i < arguments.length; i++) {
			nComplexSum += arguments[i];
		}
	});
	
	scopes.channeljs.broadcast('argTest', 1, 2, 3, 4, 5, 6);
	jsunit.assertEquals("New subscribers do not stop existing subscribers", 5, nSum);
	jsunit.assertEquals("Subcribers may be passed any number of arguments", 21, nComplexSum);
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	
	scopes.channeljs.unsubscribe(sArgChan);
	scopes.channeljs.unsubscribe(sMoreArgChan);
	
	//Test Exception handlers
	
	var sExceptionChan = scopes.channeljs.subscribe('a', function (sChannel) { throw sChannel; }, function (sException) {
		jsunit.assertEquals("Exceptions should get passed to exception handler", 'a', sException);
	})
	
	scopes.channeljs.broadcast('a');
	jsunit.assertEquals("All channels subscriber should be called once", ++nAllChannelsExpected, nAllChannelsCounter);
	
	scopes.channeljs.unsubscribe(sExceptionChan);
	scopes.channeljs.unsubscribe(sAllChan);
}

/**
 * @properties={typeid:24,uuid:"7E2DAA1C-0C06-404F-BD62-9480AD6EFF43"}
 */
function test_unsubscribe() {
	
	var bResult = scopes.channeljs.unsubscribe();
	jsunit.assertFalse("Should return false when no arguments are provided", bResult)
	
	bResult = scopes.channeljs.unsubscribe('notAChannel');
	jsunit.assertFalse("Should return false when channel not found", bResult);
	
	bResult = scopes.channeljs.unsubscribe(function notASubscriber() {});
	jsunit.assertFalse("Should return false when subscriber not found", bResult);
	
	//Set up a few subscribers to various parts of a channel
	var nCount = 0;
	var fSubscriber = function (sChannel) {
		application.output('Ubsub: [' + sChannel + ']');
		nCount++;
	}
	var sToken = scopes.channeljs.subscribe('a.b.c', fSubscriber);
	scopes.channeljs.subscribe('a.b', fSubscriber);
	scopes.channeljs.subscribe('a', fSubscriber);
	
	bResult = scopes.channeljs.unsubscribe(sToken);
	jsunit.assertTrue("Should return true when token is found and unsubscribed", bResult);
	
	scopes.channeljs.broadcast('a.b.c');
	jsunit.assertEquals("Unsubscribed channel should not receive messages", 2, nCount);
	
	bResult = scopes.channeljs.unsubscribe(fSubscriber);
	jsunit.assertTrue("Should return true when subscriber function is found and unsubscribed", bResult);
	
	nCount = 0;
	scopes.channeljs.broadcast('a.b.c');
	jsunit.assertEquals("Unsubscribed subscribers should not receive messages", 0, nCount);
}

/**
 * @properties={typeid:24,uuid:"91C30BBF-B96D-4323-92A0-D6D25BB96241"}
 */
function test_tune() {
	var sChannel = "simple";
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertEquals("", sChannel);
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertNull(sChannel);
	jsunit.assertNull(scopes.channeljs.tune(sChannel));
	
	sChannel = "a.b.c.d";
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertEquals('a.b.c',sChannel);
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertEquals('a.b',sChannel);
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertEquals('a',sChannel);
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertEquals('',sChannel);
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertNull(sChannel);
	
	sChannel = "";
	sChannel = scopes.channeljs.tune(sChannel);
	jsunit.assertNull(sChannel);
	
	sChannel = null;
	jsunit.assertNull(scopes.channeljs.tune(sChannel));
}

/**
 * @properties={typeid:24,uuid:"3FD000BE-55ED-4643-9345-6303789B23A7"}
 */
function test_getDataSourceFromChannel() {
	
	var sDataSource = 'db:/server/table';
	
	//No datasource
	var sChannel = scopes.channeljs.makeDatabroadcastChannel()
	var sTest = scopes.channeljs.getDataSourceFromChannel(sChannel);
	jsunit.assertEquals("Should return empty string", '', sTest);
	
	//No  SQLAction
	sChannel = scopes.channeljs.makeDatabroadcastChannel(sDataSource)
	sTest = scopes.channeljs.getDataSourceFromChannel(sChannel);
	jsunit.assertEquals("Should match original datasource", sDataSource, sTest);
	
	//With SQLAction
	sChannel = scopes.channeljs.makeDatabroadcastChannel(sDataSource, SQL_ACTION_TYPES.INSERT_ACTION);
	sTest = scopes.channeljs.getDataSourceFromChannel(sChannel);
	jsunit.assertEquals("Should match original datasource", sDataSource, sTest);
	
}